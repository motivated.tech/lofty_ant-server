// const MongoClient = require('mongodb').MongoClient;
// const assert = require('assert');
// const dbConfig = require('../utils/db-config.js');

// var o = function(query) {
//   let p = new Promise( function(resolve, reject){
//     MongoClient.connect(dbConfig.url, function(err, db) {
//       assert.equal(null, err);
//       let collection = db.collection('groups');
//
//       collection.find(query).toArray(function(err, docs) {
//         assert.equal(err, null);
//         resolve(docs);
//       });
//       db.close();
//     });
//   });
//   return p;
// };

const r = query => {

  const n = new Promise( (resolve, reject) => {
    resolve([
      {
        id: 'qwe',
        name: 'first',
        info: 'this is a course about stuff',
      },
      {
        id: 'rty',
        name: 'second',
        info: 'this is a course about stuff'
      },
      {
        id: 'asd',
        name: 'course 2',
        info: 'here we talk about even more stuff!'
      },
      {
        id: 'fgh',
        name: 'last',
        info: 'this is the most interesting line so far :_'
      },
      {
        id: 'QwE',
        name: 'another',
        info: 'something even more interesting...'
      }
    ]);
  });
  return n;
};

module.exports = r;
