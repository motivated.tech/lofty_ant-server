const db = 'lofty_ant';
const collection = 'groups';
const url = `mongodb://127.0.0.1:27017/${db}`;

module.exports = {  url, db, collection };
