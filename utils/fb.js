function getFbToken(authCode) {
  const querystring = require('querystring');
  const https = require('https');
  var queryData = querystring.stringify({
    client_id: '847050228720686',
    redirect_uri: 'http://localhost:4200/login',
    client_secret: '82c884e075aad324e7498619f673f84a',
    code: authCode
  });
  var options = {
    hostname: 'graph.facebook.com',
    port: 443,
    path: '/v2.7/oauth/access_token?' + queryData,
    method: 'GET'
  };
  var p = new Promise(function(resolve, reject) {
    var req = https.request(options, (res) => {
      var accesToken = null;
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        // console.log(chunk);
        accesToken = JSON.parse(chunk).access_token;
      });
      res.on('end', () => {
        resolve(accesToken);
      });
    });
    req.on('error', (e) => {
      console.log(`problem with request: ${e.message}`);
    });
    req.end();
  });
  return p;
}
