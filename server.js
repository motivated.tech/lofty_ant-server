const app = require('express')();
const cors = require('cors');

const port = 3000;

const namespace = "api";

app.use(cors());

app.get('/', function (req, res) {
  res.send('Hello World!\n');
});

app.use(`/${namespace}/users`,require('./routes/users'));
app.use(`/${namespace}/courses`,require('./routes/courses'));
app.use(`/${namespace}/groups`,require('./routes/groups'));
app.use('/token', require('./routes/token.js'));

app.listen(port, function () {
  console.log(`Lofty Ant Server is running on port ${port}!`);
});
