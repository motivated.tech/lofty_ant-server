var express = require('express');
var router = express.Router();

// Import the model for courses. In the model is where the db call occurs
var courses = require('../models/courses');

router.get('/', function (req, res) {
  res.send({courses: courses});
});

router.get('/:course_id', function (req, res) {
console.log(req.params.course_id);
  courses({id: req.params.course_id}).then(function(a) {
    res.send({course: a});
  });
});



module.exports = router;
