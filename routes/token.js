const router = require('express').Router();
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));

router.post('/', function(req, res) {
  if (req.body.grant_type === 'password') {
    if (req.body.username === 'letme' && req.body.password === 'in') {
      res.status(200).send('{ "access_token": "secret token!", "account_id": 1 }');
    } else {
      res.status(400).send('{ "error": "invalid_grant" }');
    }
  } else if (req.body.grant_type === 'facebook_auth_code') {
    if (req.body.auth_code) {
      getFbToken(req.body.auth_code).then((token) => {
        res.status(200).send({ "access_token": token, "account_id": 1 });
      });
    } else {
      res.status(400).send('{ "error": "invalid_grant" }');
    }
  } else {
    res.status(400).send('{ "error": "unsupported_grant_type" }');
  }
});

function getFbToken(authCode) {
  const querystring = require('querystring');
  const https = require('https');
  var queryData = querystring.stringify({
    client_id: '847050228720686',
    redirect_uri: 'http://localhost:4200/login',
    client_secret: '82c884e075aad324e7498619f673f84a',
    code: authCode
  });
  var options = {
    hostname: 'graph.facebook.com',
    port: 443,
    path: '/v2.7/oauth/access_token?' + queryData,
    method: 'GET'
  };
  var p = new Promise(function(resolve, reject) {
    var req = https.request(options, (res) => {
      var accesToken = null;
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        // console.log(chunk);
        accesToken = JSON.parse(chunk).access_token;
      });
      res.on('end', () => {
        resolve(accesToken);
      });
    });
    req.on('error', (e) => {
      console.log(`problem with request: ${e.message}`);
    });
    req.end();
  });
  return p;
}

module.exports = router;
