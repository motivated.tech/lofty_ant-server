// var express = require('express');
const router = require('express').Router();

// Import the model for courses. In the model is where the db call occurs
const groups = require('../models/groups');

router.get('/', (req, res) => {
  groups({})
  .then( a => res.send({ group: a }));
});

router.get('/:group_id', (req, res) => {
  groups({ id: req.params.group_id })
  .then( a =>  res.send({ group: a }));
});

module.exports = router;
